---
title:  "Преди всичко не вреди"
date:   2020-03-28
categories:
---

> "So far, there are 2,735 cases in the country and 66 deaths,
> including 16 people who have recovered."

Good. Next year the world will have 16 more Easters to celebrate for
those 16 people that have risen from death.

> 9 ноември 2009:
> Нови жертви на свинския грип, над 175 хил. болни
> Новият грип ще е силен още две години, може да има до две вълни на разпространение, според шефа на Института по микробиология при БАН
> 15-годишно момче със свински грип от Каварна, което [...] е починало тази нощ [...] 
> [...] сепсис, двустранна бронхопневмония и органна недостатъчност. Момчето не е имало съпътстващи заболявания, освен трета степен на затлъстяване.
> [...] втори пациент в МБАЛ "Рокфелер" в Петрич, съобщават от болницата.
> Медиците отбелязват, че за разлика от обикновения новият грипен вирус засяга и поразява не част от белия дроб, а целия орган.


Нямаме достатъчно информация. Не знам за вас, но аз си имам куп
причини да не вярвам на властта. Не от сега. Винаги съм си ги имал, но
сега още повече. Призивите да "вярваме само на официалната информация"
са комични. Защо точно на "официалната информация" да вярвам? И, освен
това, ако някой иска да вярвам на "официалната информация", каквото и
да значи това, нека да бъде любезен да обере официалната информация от
емоционални нотки да я направи маааалко по-балансирана и смислена, за
да заприлича малко повече на информация и малко по-малко на
предизборна реч.

Безспорно все още много неща не се знаят. Но още повече неща се
изкривяват или скриват (съзнателно или не).

И нека да кажа, в случай, че оставя място за съмнение. Аз __не__
__знам__ колко е голям проблема, какъв е и какво трябва да се направи.
__Не__ __знам__. Именно поради това пропагандирам да направим
възможното, за да предпазим възрастните хора и други със слаба имунна
система наоколо. Извадихме от праха стар кислороден концентратор,
пробвах го, изглежда работи. Купих консумативи. Игнорирах онези лекари
около мен, които ми казаха, че няма полза и послушах онези, които ми
казаха, "да, извади го". Ако някой ми каже __какво__ по-добро __мога__
да направя, слушам внимателно. Ноември месец настоявах бабите на
дъщеря ми да се ваксинират.

Не проповядвам да седим и да не правим нищо. Напротив.

Но...

Не ме интересува колко души умират на ден в Италия. Интересува ме
колко човека на ден умират в Италия сравнено с друга зима и какъв е
техния профил. Ето примерна новина (числата са измислени и имат
илюстративна цел): 

> Последните 20 години, най-лошата зима в Италия са умирали по 2000
> човека на ден, средна възраст 64, медиана 72. Последните 2 седмици
> умират по 3000, средна възраст 50, медиана 60.

Съвсем друга новина може да бъде, ако числата са ето такива, например:

> Най-лошата зима от последните 20: По 2000 дневно, средна 64,
> медиана 72, а сега са 2200, средна 67, медиана 72.

Не се опитвам да подценявам човешкия живот, но би била съвсем различна
(хипотетична) новина от предходната.

Както и от '800 човека на ден умират!'. Което не е новина а
пропаганда. Какво ми казва? Нищо. Че в Италия умират хора.

"Вземи сам да си провериш числата!" не е отговор. Няма да ги проверя.
Няма да ги проверя по две причини.

Първата е че не мога да проверявам всяка новина, която някой ми
размахва --- била тя "официална" или някаква друга. За по-лесно
игнорирам всяка новина, която очевидно има за цел (съзнателно или не)
да бъде подвеждаща. Защото са много и аз нямам ресурс да проверявам
всичките. А за повечето нямам и възможност.

Ако някой има желание да бъде вземан на сериозно да бъде любезен да
говори цивилизовано и подредено.

Втората причина е по-важна. Дори и да се окаже че има основание за
притеснение, това няма да промени факта, че новините са поднесени по
подвеждащ начин, който цели (съзнателно или не) да всява паника.

Аз не мога да кажа дали има или няма основание за мерките които взима
властта в по-голямата част от света. Не мога да кажа, защото нямам
информация, а голяма част от информацията нямам и как да я събера.

Възможно е италианската болнична система да е с по-малък капацитет и
по-зле работеща от германската. Вероятно ли е да толкова по-зле
работеща, че да има разлика от 0.3% смъртност срещу 6+% смъртност?
Според мен не много. По-вероятно ми изглежда да има разлика в
протоколите, които се използват за тестване и отчитане на резултатите.
Демек не знаем колко е смъртността нито в Италия, нито в Германия,
защото на двете места явно мерят различни работи, а ние не знаем нито
едното.

По ред признаци изглежда, че броя на прекаралите инфекцията е
многократно по-голям от публикувания. Което не е неочаквано.
Най-вероятно вируса е много нов и непознат за нашата имунна система.
Знаем че имунната система по-трудно и по-бавно се справя с непознати
неща.

Значи не знаем колко заразени има и колко от починалите са починалите
в резултат на тази инфекция и колко от комбинация от тази инфекция и
други заболявания. 

Което означава, че нямаме информация нито за смъртността, нито за
разпространението.

Изглежда, че властта в повечето случаи взима решения импулсивно. За
добро или лошо е натрапчиво усещането, че освен импулсивност има и
желание да се вземат и да се изпълняват зрелищни решения.

"Вземете мерки сега или ще умират хора!" Ами да. Всеки ден умират
хора. Това един много тъжен факт, но това беше факт и преди 4 месеца. И
преди 10 години.

И ако "вземете мерки" и ако "не вземете мерки" ще умират хора. Въпроса
е колко в единия случай и колко в другия случай.

Вероятно в следващата една година ще бъдат повече от всяка друга от
предходните двайсетина.

Колко повече ще бъдат ако "вземете мерки" и ако "не вземете мерки"? Не
знаем. Но разумните и почтени хора казват:

> Имаме няколко възможности. (A) Можем да наложим карантина. Има
> вероятност да намалим скоростта на разпространението на заразата и
> да спечелим време за да организираме лечението им и може би да
> измислим и доставим някакви медикаменти, оборудване и защитно
> облекло за лекари и персонал. Негатива е, че ще съсипем икономиката.
> Или (B), можем да се опитаме да изолираме възрастните и тези със
> слаба имунна система и да продължим да работим.
>
> Понеже няма време да направим дискусия и сме правителство и можем да
> взимаме решения, решаваме че ще направим (A), въпреки вероятно
> огромната цена, която ще се плати като икономически загуби през
> следващите години. Освен това ще се плати и много висока цена в
> човешки живот, защото ще имаме по-малко ресурс да се грижим за
> онези, за които има нужда през следващите години. Нищо чудно да има
> по-висока детска смъртност и други тъжни последствия.

Нали не сме чули подобни изречения...? Или поне аз не съм.

Чул съм: "Вземете мерки сега или ще умират хора!" 

Чуват се гласове на разум и умереност, но човек трябва много
внимателно да се вслуша за да ги чуе.

Francois Balloux, един от авторите на доклада, на който изглежда че
много правителства базират решенията си е [съвсем ясен][tech-review]:

-   Доколко модела дава резултати, които ще съвпаднат с бъдещото, не
    знаем. Има ли и в каква степен имунитет? "Anyway, the assumption
    that people cannot become infected more than once is far more
    important to the accuracy of the model than whether or not people
    comply."
-   But [the model] doesn’t say explicitly that it is the worse
    strategy. **Very few people have understood that.** (emph mine.)
    [...] Another thing to consider closely is the impact of any
    long-term strategy on the economy.

[tech-review]: https://www.technologyreview.com/s/615377/coronavirus-model-london-vaccine-suppression-imperial-deaths

Не, цитатите не са извадени от контекс. Статията е достъпна.

Francois Balloux не само подчертава, че техния модел не казва кой подход е
по-добър. Напротив, опитва се да изброи какви са рисковете и
предимствата на всеки подход.

Вероятно съвсем случайно, беше избран по-зрелищния. Не щеш ли, това
води и до по-големи спешни разходи. Да, разбирам. Това е за мое добро.
За наше добро. Всяка прилика с действителни лица и събития е случайна.

Възможно беше да чуем коментар за статията на Richard Milne във
Financial times. (За съжаление е зад paywall: [Coronavirus ‘medicine’
could trigger social breakdown][ft].)

> [Jacob Wallenberg][wallenberg] has warned governments to weigh the
> economic threat from coronavirus more heavily or risk depression,
> social unrest and a potential lost generation.
>
> [...]
>
> “There will be no recovery. There will be social unrest. There will
> be violence. There will be socio-economic consequences: dramatic
> unemployment. Citizens will suffer dramatically: some will die,
> others will feel awful,” he added.

[wallenberg]: https://en.wikipedia.org/wiki/Jacob_Wallenberg
[ft]: https://www.ft.com/content/3b8ec9fe-6eb8-11ea-89df-41bea055720b

Добре, вероятно Jacob Wallenberg е повърхностен и неопитен и затова
няма да вземаме предивид думите му. Но този коментар, който правя е
ad-hominem и грозен. "Повърхностен и неопитен". Какви квалификации...
нека да коментираме думите, а не автора им! Всъщност няма никакъв риск
хора да умират в идните месеци и години в резултат на икономическа
криза. Защото икономиката няма общо с производството на храна, горива,
и линейки. То... това са различни неща.

А освен това (все пак ad-hominem, толкова е сладко :), Jacob
Wallenberg не е експерт по темата. Какво разбира той от вируси?

Съвсем сигурно, значително по-малко от [Sucharit_Bhakdi][sucharit]. Но
и неговото [интервю и последващи коментари][youtube-pagе] остават без
отзвук в "официалните новини". (На разкошен немски с приемлив превод
на английски, а на последния материал и на български.)

[sucharit]: https://de.wikipedia.org/wiki/Sucharit_Bhakdi

Прочее, бях искрено учуден да установя, че Herr Bhakdi е прав. Въздуха
в [Италия е сравним][italy-air-q] с този в източна Европа. И е
по-близо до Полша и България, отколкото до същинската част на Европа.
Ти да видиш...

[italy-air-q]: https://www.eea.europa.eu/publications/air-quality-in-europe-2019
[youtube-pagе]: https://www.youtube.com/channel/UCgjxQLDkeoa-uJu4sE0eNrg

Добрата новина е, че изглежда този вирус е малък проблем сам по себе
си.

Лошата новина е, че това безумие заплашва да разруши, ако вече не е,
една значителна част от икономиката. Което по всяка вероятност ще убие
много повече от нас отколкото който и да вирус в последните 50, може
би дори 100 години.

А истинки лошата новина е, колкото и да ме страх дори да си го
помисля, че риск от наистина ужасни епидемии и пандемии има. И ако
тази история показва нещо, то е че властта в съвременния свят е
смехотворно неспособна да се справи с каквато и да било епидемия. Само
че на мен не ми е баш смешно.

> Ако загина от хрема,<br/>
> чумата хич да я нема.

Радой Ралин

Не знам кой е прав и кой не... дали пропагандата или Herr Bhakdi,
например.

Ако мога да обещая нещо, то е, че след като тази истерия премине,
независимо по какъв начин, никой нищо адекватно няма да направи, за да
се подготвим за потенциален __наистина__ сериозен проблем. Дано да
греша.

Ето така се става [prepper][prepper]...

[prepper]: https://en.wikipedia.org/wiki/Survivalism

-------------------------------------------

След като публикувах това, изпратих линк на няколко приятели. Един от
отговорите, които получих, имаше линк към [тази петиция][baseline-studie].

[baseline-studie]: https://www.openpetition.de/petition/online/fuehren-sie-die-baseline-studie-durch-wir-brauchen-endlich-saubere-corona-daten?language=de_DE.utf8

-------------------------------------------

А ето и още един коментар от един в приятел California (excerpt):

The funniest thing is with precious metals: the "prices" keep going
down, but if you try to buy some, you’re told "НЯМА". Local coin
stores are closed (of course – non-essential), on-line traders are all
"out of stock".

