---
title:  "Software Bits"
date:   2020-09-19
categories:
---

- 2020-09-19

    Hmm... I didn't know about [Guix][guix] to start with. Much less
    about the impressive efforts to [reduce bootstrap footprint][guix-redux]...

[guix]: https://guix.gnu.org/en/about/
[guix-redux]: https://guix.gnu.org/blog/2020/guix-further-reduces-bootstrap-seed-to-25/


- 2024-03-08

    There should be way more entries here but for some reason I forget
    to make them entries. I will try to be more diligent. Anyway...

    Cool. [Jailbreak AI Chatbots With ASCII Art][ascii-art-jailbreak]

[ascii-art-jailbreak]: https://it.slashdot.org/story/24/03/07/2123204/researchers-jailbreak-ai-chatbots-with-ascii-art
