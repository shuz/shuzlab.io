---
title:  "Vanitas Vanitatum"
date:   2020-11-09
categories: obituaries
---

> Other things being equal, it is better to be smart than to be stupid.

- Carl Sagan

Най-вълнуващата снимка, която някой, някога е правил? Може и да има
различни мнения. Аз съм уверен. [Ужасяващо малка синя
точка][pale-blue-dot]:
![Pale-Blue-Dot][pbd]

И всъщност това е изглед от сравнително близо... отдалеч и това няма
да се види... всички ние, бели и добри, черни и лоши, глупави и не
толкова, стройни, дебели, всички, всички... всички сме на тая "бледа
синя точка"...

[pale-blue-dot]: https://en.wikipedia.org/wiki/File:Pale_Blue_Dot.png
[pbd]: /assets/Pale_Blue_Dot.png "Pale Blue Dot"


[Wikipedia][wikipedia-pale-blue-dot]:
Voyager 1, which had completed its primary mission and was leaving the
Solar System, was commanded by NASA to turn its camera around and take
one last photograph of Earth across a great expanse of space, at the
request of astronomer and author Carl Sagan. The phrase "Pale Blue
Dot" was coined by Sagan himself in his reflections on the
photograph's significance, documented in his 1994 book of the same
name.

[wikipedia-pale-blue-dot]: https://en.wikipedia.org/wiki/Pale_Blue_Dot

> The Earth is a very small stage in a vast cosmic arena. Think of the
> rivers of blood spilled by all those generals and emperors so that,
> in glory and triumph, they could become the momentary masters of a
> fraction of a dot. Think of the endless cruelties visited by the
> inhabitants of one corner of this pixel on the scarcely
> distinguishable inhabitants of some other corner, how frequent their
> misunderstandings, how eager they are to kill one another, how
> fervent their hatreds.

- Carl Sagan

Carl Sagan е роден на днешния ден през 1934.


