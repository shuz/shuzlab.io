---
title:  "Ad hominem"
date:   2021-01-28
categories:
---

Every time we say **who** said something, we voice _ad hominem_. My
take is that it doesn't really matter if it is approval or
disapproval. The claim is either true or false. Either beautiful or
clumsy. Not because who the author is but because of what the claim
is. In _theory_ it shouldn't matter who the author is. But in practice
it does. There are different reasons it does.

First, it's human. We, humans, tend to believe claims made by someone
we trust. And, conversely, tend to mistrust someone we... well,
mistrust. So, the more credibility someone has in our eyes, the easier
is for us to accept what they claim. It sounds obvious but it bears
repeating because we often forget how very wrong this is. We should
examine each claim according to its merit, not according to its
author's merit. But not doing this and judging the author instead
feels oh-so-natural and easy. If we want to avoid falling into this
trap, we should mind every step. Every single step. This might be just
unrealistically expensive. Therefore... number two...

Second, and this is better justified, there is no way to check and
debug everything, so authority matters. When Einstein says "God does
not play dice" we listen to him. Now, about a century later, it seems
he was decidedly wrong. Or was he...? When the dentists we know and
trust says a tooth needs root canal treatment, we don't like it, but
we accept it. When a dentist we don't know claims the same, we might
seek a second opinion. When a well known dictator claims that he won
eighty-seven percent of the vote we are not quick to believe this.
Authority matters.

Now the media suggests that we should take Mr Bill Gates' claims at
face value. And we should be very surprised that the public does not
believe him and all sorts of comical conspiracy theories are coming
up. We all are supposed to believe that Bill Gates is now an
all-benevolent, Oskar-Schindler-type. Right.

Don't get me wrong. I am not suggesting that Bill Gates (or any other
person) "made" the "corona virus". Or that vaccines will be used to
"implant" "chips". But let's not exclude middle ground... Conspiracy
theories are fine but I am not ready to believe in the unadulterated
good intentions either. this is someone that screwed the world for
years. And mind you, he did not stop on his own accord. He slowed down
because an amazing phenomenon slowed him down. A hobbyist project
turned world-domination and crushed the might of a $1T corporation.
And no, it's not that Mr Gates or his minions transformed themselves
into nice guys overnight. They were forced to become a bit more
civilized.

Now I should become born again and start trusting in Mr Gates'
goodwill...? I shall refrain. Not because I know his famous foundation
is doing something wrong but simply because I cannot be bothered to
even check. If someone has a track record of massive abuse thereby
accumulating a huge fortune, why should I believe that he changed his
ways overnight? Maybe he did but I have more interesting things to
spend my time on. Like reading Carl Sagan. Or Douglas Adams. Or
Huckleberry Finn. Or 3Blue1Brown videos.

Somehow I would not be at all surprised if it turns out that while
donating to his foundation he made more money than he donated. From
totally unrelated activities, I know. He is not stupid, mind you.

Ad hominem? Sure, it is. So is your trust in that dentist.

You call it reputation. Track record. Authority. I call it ad hominem.
And prefer to spend my time elsewhere.
