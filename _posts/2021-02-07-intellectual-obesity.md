---
title:  "Intellectual Obesity: A pair of hypotheses"
date:   2021-02-07
categories:
---

It seems well confirmed and widely accepted that obesity follows
"western way of life". We are not quite sure what exactly western way
of life is but we know it when we see it. There are hundreds if not
thousands of studies that try to decipher the mystery. I happen to
have a bit of a different view.

When we buy our food in a modern economy we don't know the farmer that
grew that food. We don't know the person. We never met her or him or
them. If we meet the actual farmers that made the food we can assess
what sort of people they are. And we will be less likely to buy from
the sort of people we wouldn't trust. We will generally avoid buying
from someone that beats his wife or from a farm that abuses the people
that work there. We tend to avoid jerks.

When we buy from an industrial supply we can't apply this filter.
There are several different reasons why. First, we don't even know or
meet actual people that produced the food. Then most of the time this
evaluation process happens subconsciously. In the supermarket we will
be attracted to wholesomely feeling packing and product names that
feel nice. Subconsciously again but this filter quietly working in the
background is completely subverted.

Nice people produce honest food and don't use antibiotics and hormones
to fatten their cows and pigs more quickly and don't use excessive
amounts of dubious substances to grow their crops for higher yield by
the ton. What happens is that price competition will drive them
extinct. Sure, due to public pressure food standards can be instituted
and enforced but there are tens if not hundreds of ways that an
unscrupulous farmer may subvert the system. They will use a new type
of antibiotic or hormone or whatever that is not yet quite regulated
or not quite enforced. It's an arms race and the race the unscrupulous
farmer is usually a step ahead of the regulators.

What I am saying here is that in practice, statistically speaking, it
is far more reliable to judge the character of the person that grows
your food than to judge how good this food really is.

Yes, this goes against the grain of scientific principles that
authority should not be trusted but only facts. And if anything I am
generally even more extreme in my views on mistrusting authority.
Oftentimes I catch myself doubting a statement precisely **because**
its source is an authority figure. (I know this is silly and this is
why I try to guard against it. :) So it seems wrong to say that
trusting a person is better than evaluating what you're buying.

But not quite... We don't compare to ideal conditions where we would
know perfectly well how to measure the quality of our food and we can
do it. We compare two realistic options.

1. Modern western societies will enforce some standards of measuring
   food quality. Errors will be made in the actual standards and how
   comprehensive they are and the enforcement will inevitably be less
   than perfect. Sure, in civilized societies the standards will
   evolve. But so will the unscrupulous farmer's methods. Again, it's
   an arms race.

2. Alternatively when we judge the food by a proxy, errors will be
   made. A nice couple tending their garden may turn out to be
   actually not that nice and their produce may be laced with the most
   disgusting stuff. Or maybe they are actually nice and the bad stuff
   is there due to incompetence rather than ulterior motives but
   what's the difference. Again, errors will be made.

Either way errors will be made. What my hypothesis here is that maybe
option 1 statistically produces more errors than option 2. And this is
the fundamental reason why obesity follows modern western ways of
life.

The issue is exacerbated by food processing. Business making process
foods will have every incentive to use all the ways they can get away
with to make food that hijacks our evolutionary mechanisms. We need
salt to survive. And in the savanna where we evolved salt is
exceedingly rare. This is probably why we developed a supreme liking
for salt. We detect minute quantities and it suppresses bitter taste,
so if something is salty we would eat it even if otherwise it wouldn't
taste that good. Similarly with sugar and fat and therefore the salt,
sugar, and fat in all processed foods.

But why would "dishonest" food be bad for us? Well, we don't know. But
life is too darn complex. Because of all the indisputable advances in
modern biology, we tend to feel like we are getting a grasp on life.
We are not. We made great progress and I am not trying to belittle
this. I am only saying that we should not become arrogant in our
claims of understanding. Pride is cool but hubris is not. And food is
life. Food is made from living things and then eaten by living things.
And the cycle is often repeated.

Not being able to make any judgment on the people that made the food
we procure, we are collectively left with only two criteria: price and
our sense of taste and smell. And both are laughably easy to hijack by
anyone willing to do so. In fact, the market will exercise
evolutionary pressure on the participants to exactly this. Nutritional
fact labels notwithstanding. Meanwhile, effective marketing will name
and package foods in ever more ingenious ways to subvert our
subconscious evaluation process.

Not everyone, however, falls victim to this. Data on obesity is widely
available and not disputed. But data on obesity "distribution" don't
seem to be that accessible. It is easy to notice by the unaided eye
that even in the US where obese people are the majority, there are
some that are supremely fit. By genetic luck, determination,
modification to their diets, unknown reasons, or a combination of
these, some people manage to avoid the trap. So there is sort of
"weight inequality" taking place. Few people stay super fit while a
majority become horrendously obese.

Could we face a a similar phenomenon in the intellectual realm…? And
this is very slippery ground, but sometimes we have to venture into
slippery ground because the ground is.

Historically sources of information were much more important compared
to the contents of the news we were reading. We would subscribe to a
newspaper that we trust. Similar to buying food from a farmer we
trust. Or radio station. Or whatever.

Could it be the case that by reducing our trust of authority and
trying to evaluate each item its merit, we started to consume
intellectual contents that are subtly poisoned with "mental
antibiotics" and other dubious substances? And this causes massive
intellectual obesity.

And yes, some people would become supremely well informed and
intellectually fit. But the majority would become hopelessly
intellectually obese...

And no, I am not quite sure how we can test these hypotheses. Or what
should we do to alleviate the ill effects if these actually turn out
to be true. Or sort of true.

A lot more questions than answers here. But I never claimed to have
any answers.
