---
title:  "Heaven and Hell"
date:   2023-09-19
categories:
---

Let's assume, for the sake of this discussion and to keep things
trendy, that we're living in a simulation. Effectively this means
there is a god, or perhaps a lot of them. We don't even know if those
gods can be singular or plural. What their laws of physics are. Do
they have laws of physics? Do they have physics? Do they even have
mathematics? How did they pop into being? Not sure about them, but I
am very confident that in our Universe every intricate form of life
has evolved under the pressure of Natural Selection. This perspective
is important because I am also very confident that Natural Selection
always runs on copious amounts of death, pain, and suffering.

Maybe they are running this simulation precisely because they want to
find a world with less pain...? Maybe in their world there is even
more agony and misery than in ours. Seems unlikely, but what do we
know about their world?

Why do they run our simulation? Is it costly to run this simulation?
Any simulation for that matter? As simulations go there, is our one
exceptional, uniquely unremarkable, or just someone's almost-failed
high school project?

From our point of view, these gods might as well be omniscient,
omnibenevolent, and omnipotent. Hmmm... scratch that omnibenevolent.
In their world this may not hold true, but from our vantage point they
certainly are.

Hence, once we assume that we live in a simulation, we have to agree
that there are gods and that we are utterly clueless who they are,
what moves them, or why they run us.

It is therefore conceivable that part of the simulation may include
real heaven and hell waiting for us, simulees, postmortem. Such a
prospect may appear gratuitously cruel but have we not agreed that we
know nothing about their motivation. (Heck, we don't know anything
about our own motivation either...)

Now all this may seem trivial. If we indeed inhabit a simulation, then
there are gods, we know nothing about them. We don't know if the pain,
suffering, and cruelty in our world are here because pain is
inevitable, or because they are cruel, or because they don't care. We
might be nothing more than a decoration in someone's living room, akin
to the aquariums with colorful fish in some of our living rooms.

What sincerely challenges me, however, is the next question. If I
actually, really, surely knew that heaven and hell exist, would I be a
different person?

In my own past, would I have made fewer regrettable choices? I am not
sure. Would I have done more of the things I should have done? I am
not sure. Would I be a better person from now on? That too I am not
sure about.

What about you?

P.S. Technically there may be no gods. The gods that created the
simulation we revel in may have abandoned it or may have gone extinct.
Fingers crossed there's enough charge left in those batteries.

Acknowledgments and gratitude: I used ChatGPT to check and change
(hopefully improve) my English. However, none of the above was written
or directly inspired by an AI. (Except that, if we live in a
simulation, then we are all AIs, yours truly included.) I am solely
responsible for all the nonsense; don't blame poor ChatGPT.
