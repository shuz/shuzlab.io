---
title:  "Slowing Down Life "
date:   2023-09-19
categories:
---

Many have noticed that life speeds up as we grow older. This is hardly
acknowledged until around the age of 30 or 40, and it is, admittedly,
highly subjective. However, it has been observed too many times by too
many people, yours truly included, to just ignore. And the fact that
it's subjective makes it even more important to consider because, hey,
what's more important than subjective experiences?

My hypothesis is that this acceleration of life is caused by
optimization and routine. If all goes well, and we all hope it does,
we tend to optimize various aspects of our lives: our bedtime, wake-up
time, meal schedule, work schedule, caffeine schedule, bowel movement
schedule (often influenced by the former :), exercise schedule, and
our days. Understandably so. We want to do more of what we enjoy and
optimize what's unavoidable or required to support the enjoyable.

We then extend this to optimize our weeks. The week is conveniently
divided into unequal days. You may hate Sundays and love Monday, or
vice versa, but it is easy to build a schedule of what you do on what
day. So, once these patterns become ingrained, you subconsciously
start expecting that the same pattern will continue clicking, and your
mind's eye will see table tennis sessions every Wednesday at 18:00 as
far back as it can. Most importantly, for our discussion here, as far
ahead as it can.

Worse, once we've been around long enough, we will start optimizing
our months too. And the rotation of the Earth around the Sun will
naturally help us optimize our years. This is why this phenomenon
doesn't seem to usually kick in before the age of 30. It takes a
sufficient number of years to establish an annual pattern. For many of
us modern urban dwellers, annual agricultural patterns are too
distant. Seasonal weather changes do happen, but if you work indoors,
this is also a bit removed. Kids, however, and their damned school
schedule work just as well in establishing that annual pattern.
Birthdays will follow their schedule as will the seasons and Christmas
and Easter or whatever you happen to prefer to celebrate annually. Our
brains are uncannily good at picking up patterns. Just as your heart
keeps beating (we hope!) without your consciousness participating,
your mind will also, on its own accord, pick up that pattern and in
your mind, you will see that same annual pattern in retrospect and,
more ominously, in the future. All this will fly by increasingly
faster. And then you die.

Regrettably, I am not in a position to inform you how to avoid this
last unfortunate event.

But I can suggest how you can slow down the flyby speed to a crawl.
Break the optimized schedule. Start a renovation project. This will
drain your financial and scheduling resources. If your finances are
strong enough to withstand a renovation, good for you. Then start a
bigger renovation project. Or two. Change jobs. Change partners. Add
partners. Whatever.

Start a game that involves a large portion of your skin and is risky
enough and hard enough, so it disrupts your optimized schedule.

If you start a sufficiently large number of projects, some will
inevitably fail to one degree or another. And for this strategy to
work, you have to start enough projects so some fail. Or start just
one project big enough, so it fails partially or completely. Arrange
the world around you in a way that causes you to fail in some serious
ways.

You may not like it. But slow the passage of time it will.

Acknowledgments and gratitude: I used ChatGPT to check and change
(hopefully improve) my English. However, none of the above was written
or directly inspired by an AI. (Except that, if we live in a
simulation, then we are all AIs, yours truly included.) I am solely
responsible for all the nonsense; don't blame poor ChatGPT.
