---
title:  "Way North, oops, make that Northwest"
date:   2023-11-02
categories:
---

We spent about a week in Bergen. Stereotypes seem to be mostly right.
People are super-cool, quiet, non-intrusive. It is a not very sunny,
wet, magnificently beautiful place. You are not supposed to noisily
greet everyone in the supermarket or, God forbid, in the bookstore.
This would be an intrusion of privacy. Of course greet people when
greeting is due, but respect everyone's privacy. Pedestrians, making
sure it is actually safe to so, happily ignore red signals. Fridays
and Saturdays there are hoards of presumably intoxicated people
roaming the streets and making a lot of noise. It doesn't seem that
any physical violence is taking place, but a lot of noise is made,
doubt not. So the observation found in a tourist guide: "[Norway's
alcohol consumption][scandinavian-alcohol] is lower than other
Scandinavian countries and most of Europe but this is not immediately
obvious." seems to be all correct.

[scandinavian-alcohol]: https://en.wikipedia.org/wiki/List_of_countries_by_alcohol_consumption_per_capita

It is no news that Norway is enormously rich and correspondingly
expensive. To compensate how expensive food is, it is also miserable.
Supermarkets feel like giant 7/11-s full of frozen food, margarine,
and ready made packaged foods. Sure, Norway doesn't grow many oranges
but so doesn't Germany. And the ones you find in Bergen are not only
more expensive then those in Munich but of way lower quality. Butter
is just about nowhere to be found, shelves are full of margarine
instead. Marinated herring (which I happen to adore) is inedible
because is all drowned in sugar. Yes, sugar. It is sweet like herring
jam.

Did I mention the place is rich? Sure, everyone goes, they have all
hydroelectric power, all that oil, very low population density and
whatnot. Yeah, they do. So does Russia. This was scariest thought I
had while Norway. Russia's population density is half that of Norway
and Russian land is not less endowed than Norway's. Yup, Russia could
be as rich and impressive as Norway is. Could be.
