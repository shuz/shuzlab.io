---
title:  "A Glimmer of Hope"
date:   2023-11-02
categories:
---

You are a German male in 1939. You have several options: (1) wait to
be conscripted, (2) try to defect to a more civilized place, or (3)
commit suicide. You could of course object and dissent but this is
about the same as committing suicide. Whether it's worth it, is an
entirely different discussion, economically is the same as suicide.

Of course, you have no way to know upfront, but intuitively to me it
makes sense to try to defect. And the reason we are having this
discussion is for future reference. For the next Hitler or the current
Putin.

It may be not quite clear even after the fact, because there is no
reliable sources. However, it seems there were about 350,000--400,000
males that defected of which Nazi military courts convicted to death
30,000 defectors who got caught. About 23,000 were executed.

German casualties are also disputed but let's assume a figure that
seems to be in the low-middle of the disputed range: 5 million killed
out of 18 million army.

So the chance of getting caught and executed seems to be around 9%.
The chance of getting killed in the army, more than 25%. Note that
this last figure doesn't include those that were left physically
maimed. I don't underestimate psychological damage but this is hard to
measure and those defectors could also be suffering.

On purely economic grounds it was a lot more sensible to try to defect
rather than go with the flow.

And here are some links: here[1], here[3], here[3], and here[4].

[1]: https://www.quora.com/Were-there-any-German-soldiers-who-actually-defected-from-Hitlers-Nazi-Regime?share=1

[2]: https://www.bbc.co.uk/news/world-europe-52353369

[3]: https://de.wikipedia.org/wiki/Richard_Wadani

[4]: https://en.wikipedia.org/wiki/Memorial_for_the_Victims_of_Nazi_Military_Justice

And the glimmer of hope I find is that following the war on Ukraine in
February 2022 between [900,000][russian-emmigration-w] and [4
million][russian-emmigration-mt] Russians left Russia. And this
doesn't include all those that left from 2014 to 2022. So, we are
learning. Not very quickly, but we are learning. Russians in 2014-2022
seem to behave more reasonably than the Germans in 1938-1944.

[russian-emmigration-w]: https://en.wikipedia.org/wiki/Russian_emigration_following_the_Russian_invasion_of_Ukraine
[russian-emmigration-mt]: https://www.themoscowtimes.com/2022/05/06/nearly-4m-russians-left-russia-in-early-2022-fsb-a77603
