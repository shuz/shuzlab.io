---
title:  "Playing Games"
date:   2024-04-17
categories:
---

Economist will keep repeating that economy is not a zero-sum game.
Which is totally true. Four million years ago there were no artifacts
on Earth. None. Now, mere four million years later, we have roads, AC,
antibiotics, vaccines, Nietzsche, trigonometry, glass-to-metal seals,
hygiene, turbojets, anesthetics, Alexis Zorbas, ammonia production,
microprocessors, the 9th symphony, epistemology, jazz, Gödel's
incompleteness theorem, Калино Моме, understanding of natural
selection, toothbrushes, [Djelem-Djelem][djelem-djelem], vibrators, sous vide steaks,
and spread spectrum communication. All that wealth is the non-zero
part of the game of economy. So, yeah, it's definitely a non-zero sum
business.

[djelem-djelem]: https://www.youtube.com/watch?v=23dW3H9yFSU

Life is not a zero-sum game either. Four billion years ago biomass on
Earth was 0 grams. Now it is [500 to 3000 billion tonnes][biomass]
depending on how you count. Clearly very far away from a zero-sum
game. Yet for most life forms, for all intent and purpose, daily life
is a zero-sum game. You either eat someone and live to procreate or
you get eaten and die and do not procreate. No zero-sum game is more
of a zero-sum game. And by the way, it's not a game at all.

[biomass]: https://en.wikipedia.org/wiki/Biomass_(ecology)#Global_biomass
