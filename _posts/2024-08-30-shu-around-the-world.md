---
title:  "Shu Around the World"
date:   2024-08-30
categories:
---

Probably undeserved, but I have great friends that I cherish.

They seem to be entertained by sending me a particular class of
pictures. Here is a collection


![Shu in NYC][shu-in-nyc]

[shu-in-nyc]: /assets/shu-in-nyc.jpg "Shu in NYC"

![Shu in Japan][shu-in-japan]

[shu-in-japan]: /assets/shu-in-japan.jpg "Shu in Japan"

![Shu in Vienna][shu-in-vienna]

[shu-in-vienna]: /assets/shu-in-vienna.jpg "Shu in Vienna"
