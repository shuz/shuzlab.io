---
title:  "Movers and Shakers"
date:   2024-09-21
categories:
---

Nothing that I can think of better illustrates how much human moods
and feelings depend on our physical condition than having a good ---
I'll be polite here, for a change --- bowel movement.

We feel light, uplifted, elated, optimistic, and oh-so-delighted and
delightful. Small children will tend to go around literally singing
songs of joy. Adults may not quite, but the bright feelings are often
easy to notice.

Does constipation promote depression? Or perhaps vice versa...?
