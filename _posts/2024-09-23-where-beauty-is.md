---
title:  "Sometimes Beauty *is* in the Eye of the Beholder"
date:   2024-09-23
categories:
---

**TL;DR**

> Humans believe that the appearances of other humans is a good way to
> judge someone's character, IQ, sexuality, skills, and just about
> everything about the "deeper" qualities of that human. We know that
> most of the time this is not true. But we do it anyway oftentimes
> against our better judgment. (So dress well :) Still there are
> nuances that make it more complicated. Importantly the associations
> we have are plastic and can be morphed and changed. To a degree.

Background
==========

It's not news that we humans treat other humans better when they look
better. Or "are more attractive" if you will. Most of us will
emphatically deny this if asked, insisting we aren't swayed by
appearance but over and over again it has been revealed we are. All of
us. While what exactly is considered "physically attractive" is
culturally specific, the attraction to those we deem "beautiful" is
not. Countless examples from visual arts and literature reflect this.

Let's skip those and turn to the data.

In the section "What People Look for in a Partner" of Chapter 1 ("The
AI Marriage") of "Don’t Trust Your Gut" Seth Stephens-Davidowitz cites
a 2010 paper by Günter J. Hitsch, Ali Hortaçsu, and Dan Ariely that
found one answer:

> Someone Beautiful

Most attractive women have 60 percent chance of hearing back, least
attractive have 30. Most are, of course, somewhere in between. And
before you judge men as superficial (which we undeniably are), the
same ratio for men is even more pronounced: 14 vs. 36 percent. Yes, we
men are superficial. Unless compared to women.

In case you wonder, next few sections are entitled (not a full list):
- Someone Tall (If a Man)
- Someone of a Desired Race (Even if They’d Never Admit It)
- Someone Rich

In "Dataclysm: Who we are (when we think no one’s looking)" Christian
Rudder reveals similar finding, more detailed, more engagingly
written, yet less quote-friendly. (And perhaps "therefore" rather than
"but". Oh, well...)

Attractive people get more dates, more job interviews, more messages.
Most CEO are taller than average. And so on, and so on...

You may doubt it, I don't. Attractive people have it better. Always
have. The only thing that changes is who exactly is considered
attractive.

Why?
===

Yes, "why?". The question isn't asked often. Why?

My hypothesis. When asked, people claim that what they are looking for
is "deeper qualities". Dependable character, loyalty, charm,
intelligence, whatever people say. And they're not lying. It isn't
that people _say_ they want dependable character but secretly they
want good looks.

We use good looks as a shortcut. We tend to believe that people who
look well have "deeper" qualities. Look at art from old Dutch masters
to modern day. People's faces are drawn with the clear intention to
convey their character. In literature, countless descriptions of
facial feature, eyes, eye color, height, and stature aim to inform the
reader of "deeper" qualities.

[antonia-devaucay]: https://en.wikipedia.org/wiki/Portrait_of_Madame_Duvaucey

In "How to Think More about Sex" Alain de Botton talks about
Jean-Auguste-Dominique Ingres portrait of one [Madame Antonia Devaucay de
Nittis][antonia-devaucay]: 
> If we appreciate Madame Devaucay’s beauty, it is surely not just
> because we assess her as being healthy, but also because we are
> moved by her entire character as it skilfully expresses itself to us
> via the features of her face.

Even an author as balanced as de Botton admits that we judge character
by looks. And further:

> ... may even turn us on, because her face hints at a range of qualities
> besides health – qualities that (without making any claims to
> scientific accuracy) we might be able to put words to, and that we
> might welcome in a flesh-and-blood partner.

We know this is mostly not true. Most people if asked will not support
the claim that tall people are smarter or otherwise make a better CEO
material. Nonetheless most of us behave contrary to our stated
beliefs.

In fairness, to further complicate the matter, appearances may, in
fact, convey information about deeper qualities. Physical beauty may
in many ways indicate physical health and this is quite naturally a
reason to feel sexually attracted. Your predecessors did very well to
mate with beautiful members of their species over the last several
hundred million years or otherwise you wouldn't be here to read this
nonsense. Right. Except that people say they look for dependable
character not healthy partners. So we are all lying on, oh, so many
levels. Fitness depends to an extent on how much care you take of
yourself in the way if diet and exercise and this may convey
information about deeper qualities. May. But we don't quite know.
(This is worth an entry of its own to even start unpacking.) How we do
our makeup, hair, dress, shoes, headgear, tattoos could also be
telling about us internally.

Could, but mostly it doesn't, because we know how to lie. Used care
salesmen dress to evoke trust, not because they’re trustworthy. That
girl wears what she wears not because she *is* but because she wants
you to *believe* she is. And if someone tells you men don't care about
their appearances --- odds are, it's a man.

Female Attractiveness
=====================

Why focus on female attractiveness? Why not male? Well, I couldn't
care less about male attractiveness :)

But also, many (heterosexual) males readily admit they're attracted to
how a girl looks, to certain female body features (real or faked) and
certain styles. Some would profess to prefer blondes, big boobs, small
tits, tall, BBW, petite, high heels, ballerina flats, whatever.
Curiously, on this topic people freely admit their attraction to
appearances. (Well, assuming males qualify as humans. A premise I
often doubt, but never mind...)

I understand this viscerally as I am too turned on by certain female
body features, style, and even cosmetics. (For the record decidedly
_not_ by Madam Antonia-Devaucay above.) I try however to suppress
these feelings.

Here's why.

It is my belief that we (males) feel attracted to these features
(regardless if body features or styles) because we associate them with
certain character traits. 

Most often, we (males) associate body features and style with
sexuality or sexual experiences we feel attracted to. Is that simple.
I promise you. He sees a blonde. A neuron (there aren't many, it's a
dude, remember) in his brain clicks and connects the image with an
expectation of a blow job. He is attracted. Many men often profess
negative associations too. "I don't like skinny women."

Out on a Limb
=================

Everything so far is well supported by the data available. What
follows is just a hunch.

There are two ways such associations are made. Let's call them builtin
and experiential. The builtin is easy because it is undecipherable: it
may be culture, it may be childhood associations, whatever it just
there and that's it. "I like petite women." Whatever.

The experiential association is based on something that can be
remembered. Like that girl that he spent 10 years with and was giving
him those great blow jobs was redheaded (real or faked). He is now
attracted to redheads.

It has been my experience that regardless of what body features you
are attracted too a bland sexual experience with a girl that sports
some of these features will significantly decrease the attraction. And
vise versa, great sexual experiences can easily create associations to
body and styling features. It is comically unpredictable though to
*which* features an association will be created. He had a great night
with that girl. Or a great month. Will he start liking her type of
tits now? Or the type of leather sandals she used to often wear? Or
blue nail polish? Or a second toe slightly longer than the big toe, a
peculiar feature of her feet? Your guess as bad as mine. If sex with
her melted his brain though, it is very likely that he will start
loving her and built associations with her body features and style.
Some of these associations may prove very resilient. Until the next
time, the girl that sports same features proves to be sexually banal.
And the associations will weaken.

This is why I try to ignore my expectations based on appearances: time
and again those were proven unreliable.

Acknowledgments and gratitude: I used ChatGPT to check and change
(hopefully improve) my English. However, none of the above was written
or directly inspired by an AI. (Except that, if we live in a
simulation, then we are all AIs, yours truly included.) I am solely
responsible for all the nonsense; don't blame poor ChatGPT.
