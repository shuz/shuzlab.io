---
title:  "Dear Mr Sapolsky"
date:   2024-10-24
categories:
---

> TL;DR
>
> It's ridiculous to discuss if humans have free will or not without
> actually taking the trouble to define what "free will" is and how to
> determing if an object (or an animal) has "free will" or not. Once
> we spend some time the whole question become FIXME moot because it
> doesn't even seem we know what free will is.
> 
> However, for most people "free will" is very real in the model they
> have of themselves because for most people "I" is something that
> sits outside one's body, much like a "soul". That this is "not real"
> is sort of irrelevant because for most people it is real.
> 
> Mr Sapolsky's argument that "we, humans have no free will" and all
> the consequences is so obviously true on one level that is almost
> redundant. However on another level in a democratic vote it would be
> voted very much against.[^1]

[^1]: Above TL;DR did not turn out very short. I seem to lack the
     skill to make it shorter, unlike M. Pascal who noted he lacks the
     time. Or maybe I have not spent enough time of my life acquiring
     the skill... Anyway...

Dear Mr Sapolsky,

With all due respect, I am very much convinced you are wrong. Not once
but twice.

To start with, how can we argue for or against "free will" unless we
actually define what "free will" is? And to the best of my knowledge
neither you nor anyone else has provided a workable definition of
"free will". It is very much like a claim that we have --- or don't
have --- a "soul". Well, be kind to me and first tell me how do we
decide if something has "free will"? Some sort of a test. So we can
check whether something, anything, has "free will". Once we know how
to test whether an object has free will or not we may be able to
figure out if we, humans, do.

Granted, it may feel intuitively obvious what "free will" is. Sometime
it feels the same for me. Let me provide a few confusing examples that
can actually clarify the issue if we get answers. Note that all we
want here are the "intuitive" answers... so it is easy. No need to get
highfalutin philosophical.

For the sake of argument, we'll assume humans have "free will".
Whatever this may be. Do young children have free will? Do very young
infants have free will? Do fetuses have free will? If not do people
"acquire" or "grow" free will quickly in a menarche type of event or
does it grow slowly like permanent teeth. Or even like the height of a
human. A young child is small but eventually grows bigger and then
starts walking and grows taller. And if free will comes in degrees,
like height, then do some people have more free will than others, just
like some people are taller than others? And is there an upper limit
of free will? Do very old and frail people have free will? Or perhaps
there is a menopause-like process so that at old age we loose our free
will. But eventually we will be able to use some sort of hormonal
replacement therapy. Do people suffering from dementia or Alzheimer's
have free will? Do the "mentally ill" (whatever this means) have free
will?

Do primates such as chimps and bonobos have free will? Dolphins?
Elephants? Octopuses? Octopi? Dogs? Schoolmasters? Members of
Parliament? Politicians?

We can get more adventurous. Up in the mountains there are those rocks
beautifully overarching above scenery roads. It can happen that for no
discernible reason a piece of rock can splinter off and fall down. By
bad luck it can drop on top of car that happens to be moving 200
meters below it and badly injure or kill the driver and passengers.
Why did the rock fall down? Nothing prompted it. It can seem that it
has done so on its own volition. Maybe it has free will? And it is
even more interesting. Does the mountain have free will or does the
rock that splintered off have free will? Or perhaps the Universe has
free will, so almost as a consequence that rock doesn't and we do not.

For your amusement you may mostly replace "free will" above with
"soul" and the text will make about as much sense as it does now.
None.

All of the above notwithstanding, we all firmly believe in free will
or so we behave. But there is a subtle difference. Most of us, in
practice all of us, have a mental model of ourselves in which mental
model "we" are outside of our body. Recently, at a social gathering, I
overheard a conversation where someone emphatically claimed that
"Whatever I am, I am not my brain". And this is an excellent summary
of our feelings and our model of ourselves. Oftentimes subconscious
but not less effective for that. The model may be somewhat nebulous
like above: it doesn't say what exactly "I am" but it is very certain
what "I am not".

And in that model, we, humans _do_ have free will. We generally
"think" or "feel", each of us, that "I" have a "brain" and a "body".
This is so deeply ingrained that we cannot even untangle it from our
language.

We've heard this said many times, may be verbatim: "My brain doesn't
work well today." See what happens? I am sitting here living my life
but regrettably on this very day my brain is not working that well. By
the way, I also got a flat tire on "my bike" yesterday and "my leg" is
hurting too because of an old injury. So, I have a bike, a leg and a
brain. It is so obvious that we don't bother to notice it, but
linguistically each of us is sitting somewhere in a fifth dimension
and each of us "has a body" and also maybe some other "possessions",
such as a bike, a house, and maybe a spouse and some kids. Aha! And a
brain. Sometimes these devices that we own work well but sometimes
some of them fail to work that well. Especially our brains.

Either way we have to "make the most of our life". It doesn't seem to
be specific to English. Expressions vary but essentially language is
built around this mental model.[^2]

[^2]: Disclaimer: If I know anything at all it is Eurocentric,
    Mediterranean if you will. I absolutely don't know how cultures in
    Asia, for example deal with this.

Many of us know that that my "I" doesn't **have** a body and a brain.
My "I" **is** a body and a brain. But as much as we know it and we
have tons of quotidien evidence, "I" find it practically impossible to
move away from this model emotionally. Intellectually I know this is
BS, but emotionally... not at all. Excuse my oversimplification but it
is done entirely for its entertainment value: "My brain" tells me I am
a brain and a body, but "my body" tells me I "have" a brain a body.

We often feel like "I" am sitting almost outside and desperately
trying to direct "my body" and "my brain" to do what "I" want. (For
anyone that may be interested, I, personally, mostly fail.)

We know that when people get head injuries or strokes, their
personality sometimes changes. Not always because if small injuries
change personalities, it is too little to notice and big injuries
often kill the victim. But sometimes it so happens that the injury is
large enough so personality change is easily observable but not large
enough to kill the victim and such cases are well documented and well
known. We know that drugs change personalities.

Clearly if "I" were somehow "outside" my body and brain "I" wouldn't
get intoxicated by substances that my body ingests. But the "I" does.
"I" wouldn't change depending on what and when and how much my body
has eaten. But the "I" does. Intellectually most of us have no doubt
that we _are_ a body and brain. But emotionally, viscerally, we don't
believe any of this.

But the notion of free will is useful. Anyway, it cannot be extracted
out of our language for centuries, so might as well make use of it...
:)

When we and by "we" I mean the whole of "we": body, brain, liver,
stomach, small intestines, large intestines, skin and all, the whole
messy bag. So when we get excited, panicked, frightened, the part of
us that doesn't think very clearly tends to get the upper hand. And
for most of us that part tends to be waaay more powerful than that
other part that _can_ think a bit clearly but is, correspondingly, way
less powerful. If our clearly thinking but less powerful faction
(let's call it "Neocortex") is shrewd enough it doesn't try to win by
force. Instead it asks the other faction to do something innocent, for
example, to start breathing deeply and slowly. Or to take a walk. Or
perhaps to go to the gym. Or perhaps to take sauna and a cold bath.
Whatever, that powerful but not very smart part of us (let's call it
"Body") looses a lot of it's strength and now Neocortex can take over
so we don't do as many stupid things as we'd otherwise do.

And this is the blueprint of what we generally call "exercising our
free will". And it is real. Real down to the last adrenaline molecule
produced in the process.

Of course, if you are "traditionally" religious you don't need all
this. The only thing you do is change the name of "Neocortex" to
"Soul" and you are done. And then your Soul is immortal or perhaps
gets reincarnated or whatever. In fact, I'd guess this is how
religions got started. Because in all likelihood Neocortex was here
before Soul was invented. Neocortex invented the Soul, not v.v.


