---
title:  "Ouch"
date:   2024-12-12
categories:
---

TL;DR

> We are created by our genes with the singular purpose to replicate
> themselves. They've been mostly successful. There is a slight issue
> that confuses the game. Eventually the reproduction tool (us) our
> genes created got a tad too smart and now has a life of its own in
> way. Which bites. But is so nicely liberating.

No, really I have no idea what the meaning of life is. Or what the
meaning of anything else is for that matter. But I know very well what
the purpose of life is.

Our genes want to copy themselves. No "quotes" around "want". Insofar
as anything in this cute, little Universe of ours "wants" anything our
genes desperately "want" to procreate. In so wanting, for the past 4B
years they've been creating ever more sophisticated reproduction
tools.

(If you are willing to argue genes are not "conscious" and cannot
"want" anything, it'd be on you to tell me what "conscious" is and
until you do, I'll not listen to this argument. Please note, all I
want is a test. A simple repeatable, conclusive test that gets an
object and says this object is conscious at level N from 0 to 9.)

(Should you think a little deeper about what exactly does "want" mean
it quickly becomes dangerously unclear. Let's not think.)

All that talk about how mothers are bonded to their children and feel
compelled to care of them, fathers that need to provide balance, early
childhood development. All of that, all of psychology, all of your
feelings, desires, dreams, hopes. All that complexity happens because
of one push only: your genes want to copy themselves. They don't have
complex, contradictory desires and needs as we, humans, do. They have
one wish. Copy me. But they cannot do it alone. So they need to create
this incredibly complex machinery.

Anwyay... Eventually the tools got so sophisticated that the tools
started having wants of their own, other than to reproduce the genes
that they were created to reproduce. Welcome to the last couple
million years...

(Any resemblance to AI development is purely coincidental.)

So yeah, of course life has no meaning for _us_, because life has no
meaning for a sewing machine or an electric drill. Sewing machines are
created by us, humans, to sew boxer shorts and small black party
dresses, preferably in red. Because red. Electric drills are created
by us humans to drill holes in walls. Computers were created by us
humans to, well, compute stuff. Similarly human bodies were created by
our genes. And the only purpose our genes have to create our bodies
was to copy those same genes that created them, keep the fresh copy of
the genes and then dispose of the now useless bodies. Ouch.

At some point, sometimes in the last 2-4-10 or whatever millions of
years, the brains included in those bodies got sophisticated enough to
start asking very sensible questions that have no sensible answers.
Ouch.

We can be very, very certain that the Universe has been
around for about 10+ B years and life on Earth for about 3+ B years.
Give or take. We can be very certain that we humans, our bodies, are a
side effect of the genes will to reproduce.

We might be in a simulation (i.e. there is a god or gods). The
simulation per se may have meaning. We might be part of a pack of
simulations: one Universe running along another few hundreds of
simultaneously running Universes. Our may be the only one that
produced life. Or all did. Or only one produced intelligence. Or all
did and our is the slowest. Regardless. The simulation may have
meaning when looking from outside. Or maybe the Universe has not been
around for that many years and life on Earth hasn't been around for
that long and it is all simulated in the simulation so it looks like
it. Regardless. It is simulated in this way. So it looks like this. Or
maybe reality and the Universe just is.

Regardless. Either way we are just a side effect. Which _is_ very
liberating.
