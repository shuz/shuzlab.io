---
title:  "Revisiting the Austrians"
date:   2024-12-22
categories:
---

TL;DR

> Karl Popper was right. Yeah, yeah, I know you know... But reading
> some modern psychology I could _feel_ it not just know it.

First, don't get me wrong: I love Freud. Reading Freud back in my late
teens was transformative for me. But so was reading Mark Twain and
Alexander Dumas. My deepest respect goes to Herr Freud as well as
to Messrs Dumas and Twain. The latter, however, do not pretend in any
way to be "science" regardless of how we choose to understand the
word. They don't even pretend to be non-fiction. It's literature. It's
the author's understanding of the human condition and the way we,
humans, work. You may like it or not. You may agree or disagree. I like
Freud, Mark Twain, and Dumas. I dislike Hemingway.

However back then, when I was reading Herr Freud somehow I didn't find
him arrogant (though he probably was) in his unspoken claim that his
writings are "science".

Now, quite a few decades later :), reading modern psychology I am very
alive to the frustration shared by another Austrian: Herr Professor
Karl Popper:

> “I found that those of my friends who were admirers of Marx, Freud,
> and Adler were impressed by a number of points common to these
> theories, and especially by their apparent explanatory power… A
> Marxist could not open a newspaper without finding, on every page,
> confirming evidence for his interpretation of history… The Freudians
> emphasized that their theories were constantly verified by their
> ‘clinical observations.’ It began to dawn on me that this apparent
> strength was in fact their weakness.”

> — Karl Popper, *Conjectures and Refutations*

Back in the day I haven't even heard of Karl Popper or falsification.
Both Freud and Popper were taboo and not available in Russian-occupied
communist Bulgaria. I owe it to a dear early lover of mine that I had
a chance to read a pre-1944 publication of Freud that she had a copy
of.

These days while reading modern psychology, I find it really annoying
that they pile claim upon claim without any attempt to provide
evidence for even correlation not to mention causation. Then they base
their thinking on top of this pile of claims as if those claims were
already accepted as true:

- "Our deepest longing is to have stable, satisfying relationships..."
  Oh really?!? How do you know what is mine or anyone else's "deepest
  longing"?

  Because reading Mark Twain, Alexander Dumas, and Nikos Kazandzakis
  as well as soul searching in myself, I don't get the same
  impression. It seems to me that "our deepest longing" is something a
  quite a bit different.

  Maybe it is actually "longing for connection"... I am not saying
  that I know it is or it isn't. But you guys don't know either.
  
  Regardless, a lot of modern psychology and psychotherapy seems to
  talk _how_ to achieve "connection" in "relationships" as if it were
  a foregone conclusion this is what one is trying to achieve.

- Repressing, suppressing, bottling up our "emotions" causes
  addiction, illness, insomnia, and host of other ailments. (And
  perhaps hair loss too. Sorry, I couldn't resist. I know it's
  childish. Just trying to express my emotions, so I don't get
  insomnia.)

[False_pregnancy]: https://en.wikipedia.org/wiki/False_pregnancy

  Once again: how do you know? I don't doubt that psychosomatic
  illness is a thing. [False pregnancy][False_pregnancy] does exist.
  Well... pregnancy is not an illness, but to me _false_ pregnancy
  decidedly is. But:
    - Not all illness is psychosomatic.
    - We know of no test that would ascertain a condition is
      psychosomatic or not. We can make an informed guess. But even
      _after_ making that guess we often have no way to confirm or
      disprove that guess. Informed or otherwise.
    - We can't deduce what psychological condition causes a particular
      illness, assuming that we know it is psychosomatic. Which we
      don't.
    - We can't predict what needs to be done to cure a particular
      psychosomatic illness.
    
  Regardless, a lot of modern psychology and psychotherapy seems to
  talk _how_ to "express" "emotions", because else.

- Flitting thoughts. When an interesting thought forms in our mind but
  escapes us, it is because it caused anxiety in some other part of
  our psyche.

  How do you even go about proving this claim?!? I grant you that
  _probably_ _sometimes_ this is true. Even assuming this, the
  grandiose claim that this is _always_ and _undeniably_ true?
  Hardly...

  But even more important objection is how do we test this hypothesis?
  What would even in theory be a good test...?
  
- Freudian slips. Some are not, some are just slips. We often conflate
  two words or stumble when trying to pronounce one word instead of
  another even if clearly and obviously none of these are particularly
  heavily loaded. Granted, sometimes it looks Freudian. But then how
  do you know which is which...?

I don't argue all these claims are false. They are just too
irritatingly vague to even be clearly evaluated as true or not. This
is where I feel Herr Popper's irritation that, allegedly, caused him
to do what he did.

I am quick to admit few complications here.

Human psyche is undeniably vastly more complicated than physics. If we
were to apply the same rigid standard to psychology's claims as to
those of physics there would be no chance for progress. Not in the
foreseeable future anyway. So we cab perhaps live with guesses and
correlations instead of causation.

There are magnificent complications in studying human psychology.
Experiments are nearly impossible. The subject we study is carefully
guarded. Not just hidden and hard to observe as in physics but
obscured, hidden, _actively_ masked, _and_ actively and carefully
guarded. And often it would be (rightfully) considered unethical to
try to pry it open.

That's all fine and true. I respect this. And I intend to keep reading
both Freud and Mark Twain. And perhaps even modern psychology.

What's really annoying is the (sometimes implicit, but very alive)
claim that this is the Truth. No, it is not. It is just an opinion.
Maybe an informed and valuable opinion but an opinion nonetheless.

To make matters even more complicated some of their claims are either
verifiable or very plausible and evidence can be supplied. For
example, "Small children are helpless and would easily die if not
taken care of." I would suggest we refrain from arranging a
randomized, double blind, controlled study but even lacking such a
study, the claim is easy to accept. And, regrettably, throughout our
history this has been often tried with tragic consequences.

Or, "psychosomatic illness" exists. Duh.

So the whole text becomes a mixture of some very plausible and well
supported hypotheses that we can accept as true and then boatloads of
wild, sweeping claims that may or may not be true, even as plausible
as some of them sound. Mind you, this is the essential recipe of good
quality propaganda, marketing, and advertisement. And this is
irritating as apparently Herr Popper also felt.
