---
title:  "Choosing Nonsense"
date:   2025-01-01
categories:
---

TL;DR

> Amusingly we have more choice when selecting what do read and much
> less choice when deciding what to write.

[the-rant]: https://shuz.gitlab.io/2024/12/22/revisiting-the-austrians.html

A friend of mine made some (very nice) comments on a [rant I
wrote][the-rant]. And, tongue firmly in cheek, I wrote back to him:

> Абе ти що четеш глупости :)

Next morning lying in bed I thought that he could reply:

> Абе ти що пишеш глупости?

(So far he has not.)

Amusingly, the answer is because I have not a lot of choice... You
see, when "deciding" what to write you have two major constraints: you
can write about only what you really want to write about. Even before
writing a single word you have a general idea where you are going.
This is one. A second one is that you can only write what you can
write. You can't write more cleverly than you are clever. Or write
better than you write. You can jump so high. You can practice and
become better or worse. But at the moment of writing you are where you
are and that's that.

Neither of these constraints applies when you read something. You can
read anything about anything. You can read the stratospheric ideas of
Vernor Vinge even though you can hardly come up with something so
amazingly great. You can read the amazingly absurd ideas of Alek
Popov. Freud, Mark Twain, Christopher Alexander, Nikos Kazantzakis.
You can read anything and anyone unencumbered by your own limitations.
You can read what's written way better than you can write. Or way
worse.
