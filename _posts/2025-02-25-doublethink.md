---
title:  "Doublethink"
date:   2025-02-25
categories:
---

Според проучвания на "Мяра":
- 55 % са ["по-скоро привърженици на ЕС и НАТО, отколкото на Русия"][eu-nato]
- 47 % от анкетираните казват, че ["имат доверие на Румен Радев"][radev]

Това са данни от една и съща агенция. Лъже, маже, криви или каквото и
да е, агенцията е същата. Все през февруари 2025.

На теория е възможно популацията да е разделена точно на две и да няма
застъпване, 55+47 е почти 100. На практика не е възможно.

Това означава че някаква част от анкетираните успяват едновеременно да
са "по-скоро привърженици на ЕС и НАТО, отколкото на Русия" и да "имат
доверие на Румен Радев". Doublethink anyone...


[eu-nato]: https://offnews.bg/analizi/miara-pochti-dva-pati-poveche-balgari-podkrepiat-es-i-nato-otkolkoto-839042.html


[radev]: https://webcafe.bg/politika/myara-radev-zhelyazkov-i-borisov-sa-nay-populyarnite-polititsi-a-peevski-nay-nepopulyarniyat.html
