---
title:  "Fortune telling"
date:   2025-02-26
categories:
---


> Wikipedia: [Fortune telling][fortune-telling] is the unproven
> spiritual practice of predicting information about a person's life.

[fortune-telling]: https://en.wikipedia.org/wiki/Fortune-telling

Unproven?

> On some great and glorious day the plain folks of the land will
> reach their heart's desire at last, and the White House will be
> adorned by a downright moron.
>
> -- H. L. Mencken (1880 - 1956)
